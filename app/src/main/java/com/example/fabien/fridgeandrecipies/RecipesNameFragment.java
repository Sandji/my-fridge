package com.example.fabien.fridgeandrecipies;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fabien.fridgeandrecipies.data.Recipes;

import java.util.ArrayList;
import java.util.List;

/**
 * This activity is a ListFragment which contain the name of the differents dishes.
 * It is supposed to display the names of the dishes according to the elements in the fridge.
 * When you click on one dish name, it opens an other fragment on the right with some descriptions
 *
 * @author Fabien Kfoury
 *         A simple {@link Fragment} subclass.
 */
public class RecipesNameFragment extends ListFragment implements AdapterView.OnItemClickListener {

    public static final String POSITION = "position";
    public static List<Food> FoodList = MainActivity.FoodList;

    public RecipesNameFragment() {
        // Required empty public constructor
    }

    /**
     * The  first and the  second loop are made to get into FoodLIst which is the list that contains ingredients
     * And which has been implemeted thanks to the ser and retrieve with the database
     * <p>
     * The third loop whith k index is here to go in the string Array into Recipes.class and to check if it matches with Food name into the fridge
     * String elements in the ingredients have 2 forms : "one element" ,"second element" and  "one element" + "second element".
     * In the list, I check the elements names,2 by 2  .
     * The first one with the second one, then the first one with the third one until k equals to the lenght of the array
     * <p>
     * If the 2 elements names match with ingredients I add in the String array, then we append the ingredients names in the DishName List.
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //  List of String to store the Dishes
        List<String> DishName = new ArrayList<>();

        // We get the food from the database
        DatabaseHandler db = new DatabaseHandler(getContext());
        FoodList = db.getAllFood();
        db.close();
        if (!FoodList.isEmpty()) {

            // Adding to the list names of  1 ingredient dishe
            AddDIshWith1Ingredient(DishName);

            // Adding to the list names of 2 ingredients dishe
            AddDishWith2Ingredients(DishName);

            // Displaying the List DishName
            setListAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, DishName));
            getListView().setOnItemClickListener(this);
        }
    }

    /**
     * Method to add dishes with 1 ingredient.
     * We check between Recipes.Ingredients, whiwh is a String Array that I have created, with the Fridge List , FoodList,
     * which hold in Ingredients add by the user.
     *
     * @param DishName
     */
    public void AddDIshWith1Ingredient(List<String> DishName) {
        for (int i = 0; i < FoodList.size(); i++) {
            for (int j = 0; j < Recipes.Ingredients.length; j++) {
                // We compare if the foodName from the database match with the list of ingredient I added in the Recipesclass
                if (FoodList.get(i).getName().equals(Recipes.Ingredients[j])) {
                    // If it matched, we add the name of the dish corresponding to the food
                    DishName.add(Recipes.RecipesNames[j]);
                }
            }
        }
    }

    /**
     * Method to add dishes with 2 ingredients.
     * The  first and the second loops are made to get into FoodLIst which is the list that contains the fridge ingredients
     * And which has been added thanks to the user.
     * <p>
     * The third loop whith k index is here to get in the string Array into Recipes.class and to check if it matches with each ingredient name into the fridge.
     * String elements in the Recipes.Ingredients have 2 forms : "one element" ,"second element" //////  "one element" + "second element".
     * In the fridge List, FoodList, I check the elements names, 2 by 2  .
     * The first element with the second one, then the first one with the third one until k equals to the lenght of the array
     * <p>
     * If the 2 elements names match with the String Array Recipes.Ingredients , then we append the ingredients names in the DishName List.
     *
     * @param DishName List of String where we add dishes name according to what we can cook.
     */
    public void AddDishWith2Ingredients(List<String> DishName) {

        for (int i = 0; i < FoodList.size(); i++) {
            for (int IndexSeveralIngredients = 1; IndexSeveralIngredients < FoodList.size(); IndexSeveralIngredients++) {
                for (int k = 0; k < Recipes.Ingredients.length; k++) {    //    0 < k < lenght of Ingredient String Array
                    // Ingredients are stored as " ingredient1ingredient2" in the String Srray of Recipes.Ingredients
                    if ((FoodList.get(i).getName() + FoodList.get(IndexSeveralIngredients).getName()).equals(Recipes.Ingredients[k]) ) {
                        DishName.add(Recipes.RecipesNames[k]);
                    }
                }
            }

        }
    }

    /**
     * Method which open a Description Fragment on the left side of the screen according to the item selected.
     *
     * @param position Position of the item selected
     * @param id       Id of the item selected
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        // Display picture and text when we click on the list on the left
        RecipesDetailFragment detailsFragmentNew = new RecipesDetailFragment();

        /**
         * I look for the text of the selected item
         */
        String TextOfItemSelected = ((AppCompatTextView) view).getText().toString();
        /**
         * I call the function i have created to retrieve its position
         */
        int indexNameOfItemSelected = indexOf(TextOfItemSelected, Recipes.RecipesNames);

        /**
         * I send the position corresponding to the text selected
         * And open the fragment with fragmentTransaction
         * Don't forget the  fragmentTransaction.commit() otherwise, it will not work.
         */
        Bundle bundle = new Bundle();
        bundle.putInt(POSITION, indexNameOfItemSelected);
        detailsFragmentNew.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentRight, detailsFragmentNew);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     * Method which allow us to retrieve the position of a String element in an String Array.
     *
     * @param text         String element.
     * @param RecipesNames String Array which supposed to contain the String element.
     * @return Return the index(position in the Array) of the String element.
     */
    public int indexOf(String text, String[] RecipesNames) {
        int index = 0;
        for (int i = 0; i < RecipesNames.length; i++) {
            if (RecipesNames[i].equals(text)) {
                index = i;
            }
        }
        return index;
    }

    @Override
    public void onStart() {
        super.onStart();
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        getListView().setSelector(android.R.color.holo_blue_dark);
    }
}
