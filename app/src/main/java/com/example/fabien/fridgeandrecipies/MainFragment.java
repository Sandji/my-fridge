package com.example.fabien.fridgeandrecipies;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.GridView;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    public static GridView gridview;
    List<Food> FoodList = MainActivity.FoodList;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        gridview = (android.widget.GridView) rootView.findViewById(R.id.gridViewLayout);

        gridview.setAdapter(new ImageFoodAdapter(getContext(), FoodList));
        return rootView;


    }

}
