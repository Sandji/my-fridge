package com.example.fabien.fridgeandrecipies;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * @author Fabien Kfoury
 */

public class DatePickerFragmentUpdateFood extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    public static int year;
    public static int month;
    public static  int day;

    /**
     * Main method which set the year, month and day to the date of today.
     * @param savedInstanceState
     * @return a Date Picker Dialog seted with today's date.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    /**
     * This method change the intial date to a new one and change the text of the EditText
     * @param view
     * @param y Int which represent the year
     * @param m Int which represent the month
     * @param d Int which represent the day
     */
    public void onDateSet(DatePicker view, int y, int m, int d) {
        year = y;
        month = m+1;
        day = d;

        UpdateActivity.EditTexteExpirationDate.setText(d+ "/" + m + "/"+ y);

    }
}
