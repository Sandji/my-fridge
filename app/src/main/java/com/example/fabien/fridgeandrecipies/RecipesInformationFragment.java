package com.example.fabien.fridgeandrecipies;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fabien.fridgeandrecipies.data.Recipes;


/**
 * This class is a Fragment which is created when we click on a dish name
 * It contains several informations about the dish
 * A simple {@link Fragment} subclass.
 *
 * @author Fabien kfoury
 */
public class RecipesInformationFragment extends Fragment {

    public static String POSITION = "position";


    public RecipesInformationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    /**
     * I catch, with the Bundle args = getArguments() , the position of the text selected
     * And display the informations which are situated in Recipes class.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_recipes_information, container, false);

        Bundle args = getArguments();

        int position = args.getInt(POSITION);

        TextView RecipeName = (TextView) root.findViewById(R.id.RecipeName);
        RecipeName.setText(Recipes.RecipesNames[position]);

        TextView description = (TextView) root.findViewById(R.id.RecipeDescription);
        description.setText(Recipes.RecipeDescription[position]);

        ImageView imageView = (ImageView) root.findViewById(R.id.imageView);
        imageView.setImageResource(Recipes.DishPhotos[position]);

        TextView level = (TextView) root.findViewById(R.id.RecipeDifficulty);
        level.setText("Level : " + Recipes.DishLevel[position]);

        ImageView imageTime = (ImageView) root.findViewById(R.id.timeIcon);
        imageTime.setImageResource(R.drawable.ic_query_builder_black_24dp);

        TextView textTime = (TextView) root.findViewById(R.id.time);
        textTime.setText(Recipes.DishTime[position]);

        TextView textTechnique = (TextView) root.findViewById(R.id.RecipeTechnique);
        textTechnique.setText(Recipes.DishTechnique[position]);

        return root;
    }

}
