package com.example.fabien.fridgeandrecipies;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Fabien Kfoury on 10/11/2016.
 *
 * @author Fabien kfoury
 */
public class ImageFoodAdapter extends BaseAdapter {

    List<Food> FoodList;                            // List of class food
    private Context context;
    public static int pos;
    public static String Toast_Text_Test;


    public ImageFoodAdapter(Context context, List<Food> foodList) {
        this.FoodList = foodList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return FoodList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        View grid;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(context);
            grid = inflater.inflate(R.layout.activity_grid_view, null);

            TextView textViewName = (TextView) grid.findViewById(R.id.namefood);
            TextView textViewQuantity = (TextView) grid.findViewById(R.id.quantityfood);
            TextView textViewDate = (TextView) grid.findViewById(R.id.Expirationfood);

            StringBuilder messageFromAddFood = new StringBuilder();
            messageFromAddFood.append(FoodList.get(position).getName());
            textViewName.setText(messageFromAddFood);
            textViewName.setTypeface(null, Typeface.BOLD);

            textViewQuantity.setText("Quantity : " + FoodList.get(position).getQuantity());
            textViewDate.setText(FoodList.get(position).getExpiration_date());
            textViewDate.setTypeface(null, Typeface.BOLD);


            grid.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Toast_Text_Test = FoodList.get(position).getQuantity()+" "+FoodList.get(position).getName()  +" "+ FoodList.get(position).getExpiration_date();
                    Toast.makeText(context, Toast_Text_Test, Toast.LENGTH_SHORT).show();
                    //pos = position + 1;
                }
            });



            grid.setOnLongClickListener(new View.OnLongClickListener() {

                public boolean onLongClick(View v) {
                    // (Activity)context) is to refer to the superclass
                    ((Activity) context).startActivity(new Intent(context, OnLongClickActivity.class));
                    pos= position;
                    return true;
                }
            });


        } else {
            grid = (View) convertView;
        }

        return grid;
    }
}
