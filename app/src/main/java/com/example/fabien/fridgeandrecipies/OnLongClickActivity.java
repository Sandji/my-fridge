package com.example.fabien.fridgeandrecipies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.List;

/**
 * This class is used to Delete or SetUp a food attribut
 * It is a Dialog Activity
 *
 * @author Fabien kfoury
 */
public class OnLongClickActivity extends AppCompatActivity {

    List FoodList = MainActivity.FoodList;
    DatabaseHandler db = new DatabaseHandler(this);
    ImageButton btndelete;
    Button btnCancel;
    Button btnSetUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onlongclick_);

        btndelete = (ImageButton) findViewById(R.id.delete);
        btnCancel = (Button) findViewById(R.id.Cancel);
        btnSetUp = (Button) findViewById(R.id.SetUp);
    }

    /**
     * This method deals with the user clicks.
     * It delete, open a new set up activity or comes back to the current activity
     *
     * @param v
     */
    public void OnDeleteClick(View v) {
        switch (v.getId()) {
            case R.id.delete:
                db.deleteFood(FoodList, ImageFoodAdapter.pos);
                db.close();
                startActivity(new Intent(this, MainActivity.class));
                break;
            case R.id.SetUp:
                finish();
                startActivity(new Intent(this, UpdateActivity.class));
                break;
            case R.id.Cancel:
                finish();
                break;
        }
    }
}
