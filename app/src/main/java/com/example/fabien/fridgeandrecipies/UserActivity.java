package com.example.fabien.fridgeandrecipies;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

/**
 * This class allow to display the user informations :
 * Name, Last name, age, favorite dish, cooking level and picture.
 *
 * @author Fabien Kfoury
 */
public class UserActivity extends AppCompatActivity {

    public static TextView UserName;
    public TextView UserLastName;
    public ImageView imageCamera;
    public TextView UserAge;
    public TextView UserFavoriteDish;
    public TextView UserSkill;
    public Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imageCamera = (ImageView) findViewById(R.id.imageCameraUser);
        UserName = (TextView) findViewById(R.id.UserName);
        UserLastName = (TextView) findViewById(R.id.UserLastName);
        UserAge = (TextView) findViewById(R.id.TextViewAge);
        UserFavoriteDish = (TextView) findViewById(R.id.TextViewFavoriteDish);
        UserSkill = (TextView) findViewById(R.id.TextVIewLevel);

        User user;
        DatabaseHandler db = new DatabaseHandler(this);
        user = db.getUser();
        db.close();

        UserName.setText(user.getName());
        UserLastName.setText(user.getLastName());
        UserAge.setText(String.valueOf(user.getAge()));
        UserFavoriteDish.setText(user.getFavoriteDish());
        UserSkill.setText(user.getCookingLevel());

        String fname = (user.getImagePath());
        setPic(fname);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

    }

    private void setPic(String path) {
        // Get the dimensions of the View
        int targetW = (int) getResources().getDimension(R.dimen.image_height);
        int targetH = (int) getResources().getDimension(R.dimen.image_height);


        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
        imageCamera.setImageBitmap(bitmap);

    }

}
