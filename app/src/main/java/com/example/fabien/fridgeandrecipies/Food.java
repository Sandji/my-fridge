package com.example.fabien.fridgeandrecipies;

/**
 * Created by kfouryf on 10/11/2016.
 * @author Fabien
 */
public class Food {

    private String name;
    private String Quantity;
    private String expiration_date;

    /**
     * Constructor that takes the expiration date, the name and the quantity of product/ food added
     * @param expiration_date Date where the product will expire
     * @param name Name of the product, food
     * @param quantity  Number which rerepsent the quantity of product/ food added
     */

    public Food(String expiration_date, String name, String quantity) {
        this.expiration_date = expiration_date;
        this.name = name;
        Quantity = quantity;
    }


    /**
     * Constructeur without any parameter
     */
    public Food()
    {

    }

    /**
     *
     * @return A string reprensenting the food's expiration date
     */
    public String getExpiration_date() {
        return expiration_date;
    }

    /**
     *
     * @return A string reprensenting the food's name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return A string reprensenting the food's quantity
     */
    public String getQuantity() {
        return Quantity;
    }

    /**
     * A getter of Expiration_Date
     * @param expiration_date A String containing the food's expiration date
     */
    public void setExpiration_date(String expiration_date) {
        this.expiration_date = expiration_date;
    }

    /**
     * A getter of Name
     * @param name A String containing the food's name
     */
    public void setName(String name) {
        this.name = name;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    /**
     *
     * @return A string containing the name, the quantity and the expiration of the food added in the fridge
     */
    public String toString(){
        return  getName().toUpperCase() +"\n" +
                "Quantity : " +getQuantity() +"\n" +
                "Expiration date : " + getExpiration_date();
    }
}
