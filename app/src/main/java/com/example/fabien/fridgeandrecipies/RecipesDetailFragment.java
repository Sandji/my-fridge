package com.example.fabien.fridgeandrecipies;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fabien.fridgeandrecipies.data.Recipes;

import java.util.List;


/**
 * This activity is a fragment. It display Details of the dish :  Name, photo, level and the time.
 *
 * @author Fabien Kfoury
 * A simple {@link Fragment} subclass.
 */
public class RecipesDetailFragment extends Fragment {

    public static final String POSITION = "position";
    int position;

    public RecipesDetailFragment() {
        // Required empty public constructor
    }

    /**
     * In this method, thanks to Bundle args = getArguments() I catch the position of the text seleted
     * and I display it in the ImageVIew et text view.
     */
    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();

        if (args != null)
        {
            position = args.getInt(POSITION);
            TextView description = (TextView) getActivity().findViewById(R.id.description);
            description.setText(Recipes.RecipeDescription[position]);

            ImageView imageView = (ImageView) getActivity().findViewById(R.id.imageView);
            imageView.setImageResource(Recipes.DishPhotos[position]);

            TextView level = (TextView)getActivity().findViewById(R.id.level);
            level.setText("Level : "+ Recipes.DishLevel[position]);

            ImageView imageTime = (ImageView) getActivity().findViewById(R.id.timeIcon);
            imageTime.setImageResource(R.drawable.ic_query_builder_black_24dp);

            TextView textTime = (TextView)getActivity().findViewById(R.id.time);
            textTime.setText(Recipes.DishTime[position]);
        } else {
            TextView description = (TextView) getActivity().findViewById(R.id.description);
            description.setText(" Click on the recipes on the left to see decription");
        }


    }

    /**
     * This method handle the clicks of the user.
     * If the user click on the fragment, it start a new fragment which contains more description of the dish.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View root = inflater.inflate(R.layout.fragment_recipes_detail, container, false);

        LinearLayout detailsLinearLayout = (LinearLayout)root.findViewById(R.id.detailsLinearLayout);
        detailsLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                RecipesInformationFragment fragment = new RecipesInformationFragment();

                Bundle bundle = new Bundle();
                bundle.putInt(POSITION, position);
                fragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.fragment_container,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    return root;
    }

}
