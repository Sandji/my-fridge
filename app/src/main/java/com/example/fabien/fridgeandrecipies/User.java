package com.example.fabien.fridgeandrecipies;

/**
 * Created by kfouryf on 01/12/2016.
 */
public class User {
    String Name;
    String LastName;
    String ImagePath;
    int Age;
    String FavoriteDish;
    String CookingLevel;

    public User(String imagePath, String name, String lastName, int age, String favoriteDish, String cookingLevel) {
        ImagePath = imagePath;
        Name = name;
        LastName = lastName;
        Age = age;
        FavoriteDish = favoriteDish;
        CookingLevel = cookingLevel;
    }

    public User() {

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public String getFavoriteDish() {
        return FavoriteDish;
    }

    public void setFavoriteDish(String favoriteDish) {
        FavoriteDish = favoriteDish;
    }

    public String getCookingLevel() {
        return CookingLevel;
    }

    public void setCookingLevel(String cookingLevel) {
        CookingLevel = cookingLevel;
    }
}
