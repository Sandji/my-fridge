package com.example.fabien.fridgeandrecipies;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

/**
 * This class is the one which allow the user to register and to add his data into the database.
 * His informations will be retrieved  in the profile activity
 * This class is also used to capture user profile picture
 *
 * @author Fabien Kfoury
 */
public class UserRegisterActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;
    public static EditText UserName;
    public EditText UserAge;
    public EditText UserFavoriteDish;
    public RadioGroup radiogroupLevel;
    public EditText UserLastName;
    public Button AddUserBtn;
    public Toolbar toolbar;
    public boolean PictureTaken = false;
    ImageView imageCamera;
    String mCurrentPhotoPath;

    /**
     * Here is declared the toolbar, the radio group and the image view which will contain the photo
     * Three anonymous inner class have been used.
     * startActivity as always an intent as a parameter
     * The first one is for the toolbar to handle the navigation and to allow the user to come back to the Main Activity
     * The second one is to make the camera working
     * The third one is used for the button which add an User to the database and start the User Activity
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        UserAge = (EditText) findViewById(R.id.editTextAge);
        UserFavoriteDish = (EditText) findViewById(R.id.editTextFavoriteDish);
        radiogroupLevel = (RadioGroup) findViewById(R.id.radiogroupLevel);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        UserName = (EditText) findViewById(R.id.UserName);
        UserLastName = (EditText) findViewById(R.id.UserLastName);

        imageCamera = (ImageView) findViewById(R.id.imageCamera);
        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakePictureIntent();
            }
        });

        AddUserBtn = (Button) findViewById(R.id.AddUser);
        AddUserBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String level = ((RadioButton) findViewById(radiogroupLevel.getCheckedRadioButtonId())).getText().toString();
                String username = UserName.getText().toString();
                String userlastname = UserLastName.getText().toString();
                int userage = Integer.parseInt(UserAge.getText().toString());
                String userfavoritedish = UserFavoriteDish.getText().toString();

                if (/*PictureTaken && */userage != 0) {
                    Toast.makeText(getBaseContext(), "User created", Toast.LENGTH_SHORT).show();
                    DatabaseHandler db = new DatabaseHandler(getBaseContext());

                    db.addUser(mCurrentPhotoPath, username, userlastname, userage, userfavoritedish, level);
                    db.close();
                    startActivity(new Intent(getApplicationContext(), UserActivity.class));
                } else {
                    Toast.makeText(getBaseContext(), "Missing Value", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Here is the method whiwh is called when you want to open the camera activity.
     * It also create a file for the photo with the createImageFile() function.
     * Whith this file, we can obtain a path from the photo which will helps to find and display the photo.
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;

            try {
                photoFile = createImageFile();

            } catch (IOException ex) {

            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    /**
     * This method is called when the camera application is about to turn off
     * It call the function setPIc() which set the picture as a Bitmap with its paths
     *
     * @param requestCode Code used when the user click on the button save of  the camera activity to know when to result the camera app
     * @param resultCode  Code used when the user click on the button save of  the camera activity to know when to result the camera app
     * @param data        Intent which comes from dispatchTakePictureIntent() method
     */


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            setPic();
            //PictureTaken  = true;
        }
    }

    private File createImageFile() throws IOException {
        String imageFileName = "JPEG_photo_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = imageCamera.getWidth();
        int targetH = imageCamera.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        imageCamera.setImageBitmap(bitmap);
    }

}
