package com.example.fabien.fridgeandrecipies;

import android.content.Intent;
import android.graphics.*;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shamanland.fab.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author Fabien Kfoury
 */

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    public FloatingActionButton fab;

    public boolean PictureSave = false;

    public static List<Food> FoodList;
    public static int position ;              // Pour savoir la positin de l'item cliké

    NavigationView navigationView = null;
    Toolbar toolbar=null;

    /**
     * onCreate method : Used here to :
     * Display the Drawer in the toolbar
     * Display the fab Button (Floating Action Button)
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.navi_view);
        navigationView.setNavigationItemSelectedListener(this);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.initBackground();
        final DatabaseHandler db = new DatabaseHandler(this);

        FoodList = db.getAllFood();                                 // getAllFood goes in the DatabaseHandler
        db.close();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MainFragment fragment = new MainFragment();
        fragmentTransaction.replace(R.id.fragment_container,fragment);
        fragmentTransaction.commit();
    }

    public void OnButtonClick(View v)
    {
        switch(v.getId()) {
            case R.id.fab :
                startActivity(new Intent(this, Add_Food.class));
                break;
        }
    }

@Override
public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
        drawer.closeDrawer(GravityCompat.START);
    } else {
        super.onBackPressed();
    }
}

    /**
     * Inflate the menu.
     * This adds items to the action bar if it is present.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    /**
     * This method handle the cliks on the setting button situated in the toolbar
     * @param item This is the item selected by the user
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * This method is used to navigate in the drawer and to change to one fragment to the other
     * Thanks to the id setted up in the XML document, we can know which item has been selected
     * @param item This is the item selected by the user
     * @return
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (id == R.id.nav_fridge) {
            Toast.makeText(getBaseContext(), "My fridge", Toast.LENGTH_SHORT).show();
            MainFragment fragment = new MainFragment();
            fragmentTransaction.replace(R.id.fragment_container,fragment);


        } else if (id == R.id.nav_recipes) {
            Toast.makeText(getBaseContext(), "Recipes", Toast.LENGTH_SHORT).show();
            RecipesFragment fragment = new RecipesFragment();
            fragmentTransaction.replace(R.id.fragment_container,fragment);

        } else if (id == R.id.nav_register) {
            startActivity(new Intent(this,UserRegisterActivity.class));
        }
        else if (id == R.id.nav_profil){
            startActivity(new Intent(this,UserActivity.class) );;

        }

        fragmentTransaction.commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
