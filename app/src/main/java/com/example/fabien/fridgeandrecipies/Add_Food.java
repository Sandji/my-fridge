package com.example.fabien.fridgeandrecipies;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

/**
 * Class used to add food to the fridge with  Dialog Style
 * Containing and Spinner and a DatePicker
 *
 * @author Fabien kfoury
 */
public class Add_Food extends AppCompatActivity {             // For the DatePicker

    public static final String KEY_NAME = "keyname";
    public static final String KEY_QUANTITY = "keyquantity";                      // Key are here for the putExtra
    public static final String KEY_DATE = "keyexpirationdate";
    public static EditText EditTexteExpirationDate;
    public static String selectionAutoComplete;

    private Spinner spinnerQuantity;
    private EditText EditTextName;
    private String[] NumberQuantity;
    private String Quantity;

    public static List<Food> FoodList;

    /**
     * In this create methode , we have an spinner which show us the quantity of the element to add.
     * The spinner in under setOnItemSelectedListener with the onCLick function in an anonymous inner class.
     * It is a annonymous inner class because becaue it has been declared without a class name.
     * We also have an Date Picker which is used to set the Expiration Date. onClick function is also in an anonymous class
     * <p>
     * Finaly, I choose to add an AutoCompleteTextView which helps the user to write the ingredient name
     *
     * @param savedInstanceState is to save data . Used for instance when we change orientation
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__food);

        spinnerQuantity = (Spinner) findViewById(R.id.spinnerQuantity);
        EditTextName = (EditText) findViewById(R.id.autoCompleteTextView1);

        NumberQuantity = getResources().getStringArray(R.array.NumberQuantity);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, NumberQuantity);
        spinnerQuantity.setAdapter(arrayAdapter);
        spinnerQuantity.setOnItemSelectedListener(

                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Quantity = NumberQuantity[position];
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        Quantity = "0";
                    }
                }
        );

        final Calendar c = Calendar.getInstance();
        EditTexteExpirationDate = (EditText) findViewById(R.id.expirationDATE);
        EditTexteExpirationDate.setHint(c.get(Calendar.DAY_OF_MONTH) + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.YEAR));

        EditTexteExpirationDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showDatePickerDialog(v);
            }
        });
        String[] IngredientsNames = getResources().getStringArray(R.array.Ingredients_Names);
        ArrayAdapter<String> ingredientAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, IngredientsNames);      // AutoComplete
        AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
        textView.setAdapter(ingredientAdapter);

        textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                Log.i("SELECTED TEXT WAS--->", (String) parent.getItemAtPosition(position));
                selectionAutoComplete = (String) parent.getItemAtPosition(position);        // Récupère la valeur du autocomplete
                //TODO Do something with the selected text
            }
        });

    }

    /**
     * Method which allow to send the food parameter to the database and start the mainActivity only if the values have been all initialised
     * The intent is declare from Add_Food Activity to MainActivity.
     * With intent.putExtra we send the data to the other activity.
     * To collect the date, you need to use intent.getExtra
     * Then, we add the elements to the database thanks to the DatabaseHandler by calling the addFood function.
     *
     * @param v View for the OnClick
     */
    public void onAddClick(View v) {
        if (!EditTextName.getText().toString().matches("") && DatePickerFragmentAddFood.day != 0) {

            DatabaseHandler db = new DatabaseHandler(this);
            FoodList = db.getAllFood();
            int compteur = 0;
                for(int i=0; i<FoodList.size();i++)
                {
                    if(EditTextName.getText().toString().matches(FoodList.get(i).getName() ) ) {
                        compteur ++;
                    }
                }
            if(compteur == 0) {
                Intent intent = new Intent(Add_Food.this, MainActivity.class);

                String Date = DatePickerFragmentAddFood.day + "/" + DatePickerFragmentAddFood.month + "/" + DatePickerFragmentAddFood.year;

                intent.putExtra(KEY_QUANTITY, Quantity);

                intent.putExtra(KEY_DATE, Date);
                intent.putExtra(KEY_NAME, selectionAutoComplete);

                db.addFood(new Food(Date, selectionAutoComplete, Quantity));
                db.close();
                startActivity(intent);
            }
            else
            {
                Toast.makeText(getBaseContext(), "Ingredient already in the fridge", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getBaseContext(), "Missing Value", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Method which open the Dialog Fragment including the Date Picker
     *
     * @param v
     */
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragmentAddFood();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }


}
