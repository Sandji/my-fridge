package com.example.fabien.fridgeandrecipies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.List;

/**
 * This class is the first activity the user sees.
 * It is the launcher activity
 * Only contain  a button to go to the main activity and a fridge picture
 * @author Fabien Kfoury
 */
public class FirstLauncher extends AppCompatActivity {

    List<Food> Foodlist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_launcher);

        final DatabaseHandler db = new DatabaseHandler(this);

        Foodlist = db.getAllFood();                                 // getAllFood goes in the DatabaseHandler
        db.close();
        //UserActivity.UserName.getText().toString();
        if(Foodlist.size() != 0 /*|| UserActivity.UserName.getText() != null*/)
        {
            startActivity(new Intent(this, MainActivity.class));
        }
    }

    /**
     * Allow to go to the Main Activity
     * @param v
     */
    public void CreateFridge(View v) {
        startActivity(new Intent(this, MainActivity.class));
    }
}
