package com.example.fabien.fridgeandrecipies.data;

import com.example.fabien.fridgeandrecipies.R;

/**
 * This class contains severals string array which are used to feed the Recipes activity
 * Each dish has a name, ingredients, descriptions, a level, a photo, a time and further informations
 * Created by Fabien kfoury on 28/11/2016.
 * @author Fabien kfoury
 */

public class Recipes {

        public static String[] RecipesNames ={
                "Tomato Salad",
                "Corn",
                "Corn and tomato",
                "Tomato and Corn",
                "Hummous and cucomber",
                "Pasta with chicken",
                "Tomato mozzarella salad",
                "Meatballs",
                "Warm goat cheese with honey"
        };

        public static String[] RecipeDescription= {
                "Tomato salad very good",
                "Excellent",
                "Excellent",
                "",
                "Tasty !",
                "Amazing taste",
                "Verry refreshing",
                "Tasty !",
                "Soo goood !"
        };

        public static String[] DishLevel = {
                "Easy",
                "Tricky",
                "Medium",
                "Medium",
                "Hard",
                "Medium",
                "Hard",
                "Medium",
                "Easy"
        };

        public static String [] Ingredients = {
                "Tomato",
                "Corn",
                "Tomato"+"Corn",
                "Corn"+"Tomato",
                "Hummus"+"Cucumber",
                "Pasta"+"Chicken",
                "Tomato"+"Mozzarella",
                "Goat cheese" + "Minced meat",
                "Goat cheese"+"Honey",
        };

        public static int[] DishPhotos ={
                R.drawable.tomatosalad,
                R.drawable.corn,
                R.drawable.tomatocorn,
                R.drawable.tomatocorn,
                R.drawable.hummuscucumber,
                R.drawable.pastachicken,
                R.drawable.tomatomozzarellasalad,
                R.drawable.meatballs,
                R.drawable.warmgoatcheese,
        };
        public static String[] DishTime ={
                "5",
                "3",
                "5",
                "10",
                "10",
                "15",
                "10",
                "25",
                "10",
        };

        public static String[] DishTechnique={
                "1) 2 1/4 pounds mixed ripe tomatoes, different shapes and colors.\n" +
                        "2)Sea salt and freshly ground black pepper.\n" +
                        "3) A good pinch dried oregano.\n" +
                        "4) Red wine or balsamic vinegar.\n" +
                        "5) Extra-virgin olive oil.\n" +
                        "6) 1 clove garlic, peeled and grated.\n" +
                        "7) 1 fresh red chile, seeded and chopped.",
                "",
                "",
                "",
                "",
                "",
                "",
                "Season the contents with sugar, salt and freshly ground pepper. \n" +
                        "Cook the tomatoes uncovered for approximately 30 minutes or until the tomato softens (while this is cooking make the meatballs). \n" +
                        "Heat a frying pan and cook the meatballs for about 10 minutes in about three tablespoons of olive oil.\n",

                "At the same time, toast the bread under the broiler as well for 2-4 minutes until browned on the edges. \n" +
                        "Remove bread from oven and rub with a raw clove of garlic that has been cut in half. \n" +
                        "Drizzle honey on warm goat cheese. Sprinkle on fresh thyme leaves, sea salt & fresh cracked pepper.\n"

        };
}
