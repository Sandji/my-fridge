package com.example.fabien.fridgeandrecipies;

import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

public class UpdateActivity extends AppCompatActivity {

    public static EditText EditTexteExpirationDate;
    public static String selectionAutoComplete;
    DatabaseHandler db = new DatabaseHandler(this);
    List<Food> FoodList = MainActivity.FoodList;
    private Spinner spinnerQuantity;
    private String[] NumberQuantity;
    private String Quantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        spinnerQuantity = (Spinner) findViewById(R.id.spinnerQuantityUpdate);
        NumberQuantity = getResources().getStringArray(R.array.NumberQuantity);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, NumberQuantity);
        spinnerQuantity.setAdapter(arrayAdapter);
        spinnerQuantity.setOnItemSelectedListener(

                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Quantity = NumberQuantity[position];

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        Quantity = "0";
                    }
                }
        );

        final Calendar c = Calendar.getInstance();
        EditTexteExpirationDate = (EditText) findViewById(R.id.expirationDATEUpdate);
        EditTexteExpirationDate.setHint(c.get(Calendar.DAY_OF_MONTH) + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.YEAR));

        EditTexteExpirationDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showDatePickerDialog(v);
            }
        });
        String Date = DatePickerFragmentAddFood.day + "/" + DatePickerFragmentAddFood.month + "/" + DatePickerFragmentAddFood.year;

        String[] IngredientsNames = getResources().getStringArray(R.array.Ingredients_Names);

        ArrayAdapter<String> ingredientAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, IngredientsNames);      // AutoComplete
        AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewUpdate);
        textView.setAdapter(ingredientAdapter);

        textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                Log.i("SELECTED TEXT WAS--->", (String) parent.getItemAtPosition(position));
                selectionAutoComplete = (String) parent.getItemAtPosition(position);        // Récupère la valeur du autocomplete
                //TODO Do something with the selected text

            }
        });

    }

    public void onUpdateCLick(View v) {

        int position = ImageFoodAdapter.pos;
        String Date = DatePickerFragmentUpdateFood.day + "/" + DatePickerFragmentUpdateFood.month + "/" + DatePickerFragmentUpdateFood.year;

        if (selectionAutoComplete == null || Date == "0/0/0") {
            Toast.makeText(getBaseContext(), "Missing Value", Toast.LENGTH_SHORT).show();
        } else {
            db.onUpdate(selectionAutoComplete, Date, Quantity, position, FoodList);
            db.close();
            startActivity(new Intent(this, MainActivity.class));
        }
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragmentUpdateFood();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
}


