package com.example.fabien.fridgeandrecipies;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.fabien.fridgeandrecipies.data.Recipes;


/**
 * A simple {@link Fragment} subclass.
 */


public class RecipesFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static final String STATE_COUNTER = "counter";
    private int mCounter;

    public RecipesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getChildFragmentManager().beginTransaction().add(R.id.fragmentLeft, new
                    RecipesNameFragment()).commit();
            getChildFragmentManager().beginTransaction().add(R.id.fragmentRight, new
                    RecipesDetailFragment()).commit();
        } else {
            mCounter = savedInstanceState.getInt(STATE_COUNTER, 0);
        }
        // Inflate the layout for this fragment
        View FragmentRecipe = inflater.inflate(R.layout.fragment_recipes, container, false);

        return FragmentRecipe;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_COUNTER, mCounter);

    }


}
