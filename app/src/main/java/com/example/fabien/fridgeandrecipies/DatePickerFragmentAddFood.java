package com.example.fabien.fridgeandrecipies;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

/**
 * Created by Fabien kfoury on 06/11/2016.
 * This class is a DialogFragment used to display and set up the Date Picker.
 * @author Fabien kfoury
 */

public class DatePickerFragmentAddFood extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public static int year;
    public static int month;
    public static  int day;

    /**
     * Main method which set the year, month and day to the date of today.
     * @param savedInstanceState
     * @return a Date Picker Dialog seted with today's date.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    /**
     * This method change the intial date to a new one and change the text of the EditText
     * @param view
     * @param y Int which represent the year
     * @param m Int which represent the month
     * @param d Int which represent the day
     */
    public void onDateSet(DatePicker view, int y, int m, int d) {
        year = y;
        month = m+1;
        day = d;

      Add_Food.EditTexteExpirationDate.setText(d+ "/" + m + "/"+ y);

    }
}
