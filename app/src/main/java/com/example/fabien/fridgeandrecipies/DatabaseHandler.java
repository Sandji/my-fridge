package com.example.fabien.fridgeandrecipies;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.TextView;

import com.example.fabien.fridgeandrecipies.Food;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used handle the Database and his action / queries
 * I have created 2 tables
 * Table Food and Table User
 * Created by Fabien kfoury on 11/11/2016.
 *
 * @author Fabien kfoury
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Food_Added";

    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_FOOD = "food";
   // private static final String TABLE_PHOTO = "photo";
    private static final String TABLE_USER = "user";

   // private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_QUANTITY = "quantity";
    private static final String KEY_DATE = "expirationDate";

    private static final String KEY_PATH = "path";
    private static final String KEY_LASTNAME = "lastname";
    private static final String KEY_AGE = "age";
    private static final String KEY_FAVORITE_DISH = "favoriteDish";
    private static final String KEY_COOKING_LEVEL = "cookingLevel";

    private static final String CREATE_TABLE_FOOD = "CREATE TABLE "
            + TABLE_FOOD + "(" + KEY_NAME + "," + KEY_QUANTITY
            + "," + KEY_DATE + ")";

    //private static final String CREATE_TABLE_PHOTO = "CREATE TABLE "
           // + TABLE_PHOTO + "(" + KEY_NAME + "," + KEY_ID + ")";

    private static final String CREATE_TABLE_USER = "CREATE TABLE "
            + TABLE_USER + "(" + KEY_NAME + "," + KEY_LASTNAME + ","
            + KEY_PATH + "," + KEY_AGE + "," + KEY_FAVORITE_DISH
            + "," + KEY_COOKING_LEVEL + ")";

    /**
     * This is the constructor of the class which takes the Context as a parameter
     *
     * @param context
     */
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Methode overrided whiwh allow to create the Tables
     *
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_FOOD);
        //db.execSQL(CREATE_TABLE_PHOTO);
        db.execSQL(CREATE_TABLE_USER);

    }

    /**
     * This method is used if you want to delete and recreate all the tables
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FOOD);
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_PHOTO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        onCreate(db);
    }

    /**
     * Only the delete and recreate the User's table
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    public void onUpgradeTableUSER(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL(CREATE_TABLE_USER);
    }

    /**
     * This method is used to add an Food object to the database.
     * Use log.d to show if the elements have well been inserted
     *
     * @param food Food object contains a name, an expiration date and a quantity
     */
    public void addFood(Food food) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, food.getName());         // Lower case pour que les noms soit tous du meme format
        values.put(KEY_DATE, food.getExpiration_date());
        values.put(KEY_QUANTITY, food.getQuantity());

        long Insertion_ok = sqLiteDatabase.insert(TABLE_FOOD, null, values);

        if (Insertion_ok > 0) {
            Log.d("dbhelper", "inserted successfully");
        } else {
            Log.d("dbhelper", "failed to insert");
        }
        sqLiteDatabase.close();
    }

    /**
     * Method which delete an Food object from the database
     * Belongs to the CRUD ( CREATE, READ, UPDATE, DELETE )
     *
     * @param food     Food object
     * @param position Int element which helps to know which item we have to remove from the gridView
     */
    public void deleteFood(List<Food> food, int position) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        //sqLiteDatabase.delete(TABLE_FOOD,KEY_NAME +"=" + food.get(position).getName() + "and" + KEY_QUANTITY + "=" + food.get(position).getQuantity() + "and" + KEY_DATE+ "=" + food.get(position).getExpiration_date(),null);
        sqLiteDatabase.delete(TABLE_FOOD, KEY_NAME + "= ? and " + KEY_QUANTITY + "= ?", new String[]{food.get(position).getName(), food.get(position).getQuantity()});

    }

    /**
     * This getAllFood method is used to retrieve data from the database and add them to a List thanks to a SQLite query
     * It is a list of Food ojects
     *
     * @return A List of Food objects called FoodList
     */
    public List<Food> getAllFood() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_FOOD, new String[]{KEY_NAME, KEY_QUANTITY, KEY_DATE}, null, null, null, null, null, null);
        List<Food> FoodList = new ArrayList<>();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Food food = new Food();
                food.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                food.setExpiration_date(cursor.getString(cursor.getColumnIndex(KEY_DATE)));
                food.setQuantity(cursor.getString(cursor.getColumnIndex(KEY_QUANTITY)));
                FoodList.add(food);
            }
        }
        // return All Employees
        return FoodList;
    }

    public int onUpdate(String name, String Date, String quantity, int position, List<Food> food) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_NAME, name);
        values.put(KEY_DATE, Date);
        values.put(KEY_QUANTITY, quantity);

        long updateQuery = sqLiteDatabase.update(TABLE_FOOD, values, KEY_NAME + "= ?", new String[]{food.get(position).getName()});

        if (updateQuery > 0)
            return 1;
        else
            return 0;
    }

    /**
     * Fonction to add an user in the table
     * Use  log.d to show if the elements have well been inserted
     *
     * @param photoPath    Is a String containing the path where the photo is located
     * @param Username     Is a string containing the user name
     * @param Userlastname Is a string containing the user last name
     * @param Age          Int which contain the user age
     * @param favoriteDish String which contain the user favorite dish
     * @param CookinLevel  String which contain the user cooking level
     */
    public void addUser(String photoPath, String Username, String Userlastname, int Age, String favoriteDish, String CookinLevel) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        onUpgradeTableUSER(sqLiteDatabase, DATABASE_VERSION, DATABASE_VERSION);
        //onUpgrade(sqLiteDatabase,DATABASE_VERSION,DATABASE_VERSION);
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, Username);
        values.put(KEY_LASTNAME, Userlastname);
        values.put(KEY_PATH, photoPath);
        values.put(KEY_AGE, Age);
        values.put(KEY_FAVORITE_DISH, favoriteDish);
        values.put(KEY_COOKING_LEVEL, CookinLevel);

        long Insertion_ok = sqLiteDatabase.insert(TABLE_USER, null, values);

        if (Insertion_ok > 0) {
            Log.d("dbhelper", "inserted successfully");
        } else {
            Log.d("dbhelper", "failed to insert");
        }
        sqLiteDatabase.close();
    }

    /**
     * Query to to obtain every User in the table
     *
     * @return A user which is an object containing a Name,a Lastname, an ImagePAth, an age, a Favorite Dish and a cooking level
     */
    public User getUser() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT  * FROM " + TABLE_USER, null);

        User user = new User();
        if (cursor != null) {
            cursor.moveToFirst();
            user.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
            user.setLastName(cursor.getString(cursor.getColumnIndex(KEY_LASTNAME)));
            user.setImagePath(cursor.getString(cursor.getColumnIndex(KEY_PATH)));
            user.setAge(cursor.getInt(cursor.getColumnIndex(KEY_AGE)));
            user.setFavoriteDish(cursor.getString(cursor.getColumnIndex(KEY_FAVORITE_DISH)));
            user.setCookingLevel(cursor.getString(cursor.getColumnIndex(KEY_COOKING_LEVEL)));
        }
        return user;
    }

}
