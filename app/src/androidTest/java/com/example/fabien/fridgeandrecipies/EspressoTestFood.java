package com.example.fabien.fridgeandrecipies;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.matcher.RootMatchers;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

/**
 * Created by kfouryf on 08/12/2016.
 */
public class EspressoTestFood {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(
            MainActivity.class);


    @Test
    public void Add_Food(){


       onView(withId(R.id.fab)).perform(click());

        onView(withId(R.id.autoCompleteTextView1)).perform(typeText("To"));

        onView(withText("Tomato"))
                .inRoot(isPlatformPopup())
                .perform(click());

        onView(withId(R.id.spinnerQuantity)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("2"))).perform(click());

        onView(withId(R.id.expirationDATE)).perform(click());
        onView(withId(android.R.id.button1)).perform(click());
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        onView(withId(R.id.btnAdd)).perform(click());

        onData(anything()).inAdapterView(withId(R.id.gridViewLayout)).atPosition(0).
                onChildView(withId(R.id.namefood)).perform(click());

        MainActivity activity = activityTestRule.getActivity();
        onView(withText(ImageFoodAdapter.Toast_Text_Test)).
                inRoot(withDecorView(not(is(activity.getWindow().getDecorView())))).
                check(matches(isDisplayed()));

    }
}
