package com.example.fabien.fridgeandrecipies;

import android.support.test.InstrumentationRegistry;
//import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.DatePicker;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;

/**
 * Created by Fabien on 06/12/2016.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class EspressoTestRegister {


    @Rule
    public ActivityTestRule<UserRegisterActivity> activityTestRule = new ActivityTestRule<>(
            UserRegisterActivity.class);

    /**
     * Test the creation of an user by adding user information and by checking in the profile activity if everything have been added
     */
    @Test
    public void EditUser()
    {
        onView(withId(R.id.UserName)).perform(click());
        onView(withId(R.id.UserName)).perform(typeText("Fabien"), closeSoftKeyboard());

        onView(withId(R.id.UserLastName)).perform(click());
        onView(withId(R.id.UserLastName)).perform(typeText("Kfoury"), closeSoftKeyboard());

        onView(withId(R.id.editTextAge)).perform(click());
        onView(withId(R.id.editTextAge)).perform(typeText("20"), closeSoftKeyboard());

        onView(withId(R.id.editTextFavoriteDish)).perform(click());
        onView(withId(R.id.editTextFavoriteDish)).perform(typeText("Raclette"), closeSoftKeyboard());

        onView(withId(R.id.radioButtonok)).perform(click());

        onView(withId(R.id.AddUser)).perform(click());

        onView(withId(R.id.UserName)).check(matches(withText("Fabien")));

        onView(withId(R.id.UserLastName)).check(matches(withText("Kfoury")));

        onView(withId(R.id.TextViewAge)).check(matches(withText("20")));

        onView(withId(R.id.TextViewFavoriteDish)).check(matches(withText("Raclette")));

        onView(withId(R.id.TextVIewLevel)).check(matches(withText("Not that bad")));

    }


}
